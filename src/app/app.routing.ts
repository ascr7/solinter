import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { LoginComponent } from './views/login/login.component';
import { ErrorComponent } from './error/error.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full', },
  {
    path: '',
    component: DefaultLayoutComponent,
    children: [
      {
        path: 'inicio',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'empresas',
        loadChildren: () => import('./views/clients/clients.module').then(m => m.ClientsModule)
      },
      {
        path: 'usuarios',
        loadChildren: () => import('./views/users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'visitas',
        loadChildren: () => import('./views/visits/visits.module').then(m => m.VisitsModule),
      },
      {
        path: 'materiales',
        loadChildren: () => import('./views/materials/materials.module').then(m => m.MaterialsModule)
      },
      {
        path: 'herramientas',
        loadChildren: () => import('./views/tools/tools.module').then(m => m.ToolsModule)
      },
      {
        path: 'maquinaria',
        loadChildren: () => import('./views/machinaries/machinaries.module').then(m => m.MachinariesModule)
      },
      {
        path: 'obra',
        loadChildren: () => import('./views/workforce/workforce.module').then(m => m.WorkforcesModule)
      },
    ]
  },
  {
    path: '**',
    component: ErrorComponent,
    data: { title: 'error' }
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
