import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from "./app.component";
// Import containers
import { DefaultLayoutComponent } from "./containers";
import { LoginComponent } from "./views/login/login.component";
const APP_CONTAINERS = [DefaultLayoutComponent];
import { NgxFileDropModule } from "ngx-file-drop";
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule
} from "@coreui/angular";

// Import routing module
import { AppRoutingModule } from "./app.routing";

// Import 3rd party components
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module

import { TabsModule } from "ngx-bootstrap/tabs";
import { ProductService } from "./services/product.service";
import { ConfigService } from "./services/config-service";
import { DataService } from "./services/data.service";
import { LoginService } from "./services/login.service";
import { PuntoventaService } from "./services/puntoventa.service";
import { RolesService } from "./services/roles.service";
import { VentasService } from "./services/ventas-service";
import { CartService } from "./services/cart-service";
import { UserIdleModule } from "angular-user-idle";
import { ClientService } from "./services/client.service";
import { UserService } from "./services/user.service";
import { PromocionService } from "./services/promocion.service";
import { SchedulesvisitsService } from "./services/schedulesvisits.service";
import { MaterialsService } from "./services/materials.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ErrorComponent } from "./error/error.component";
import { PaginServiceService } from "./services/pagin-service.service";
import { NgxLoadingModule } from "ngx-loading";
import { AgGridModule } from "ag-grid-angular";
import { ToastrModule } from 'ngx-toastr';
import 'hammerjs';
import 'mousetrap';
import { GalleryModule } from '@ks89/angular-modal-gallery';


@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppAsideModule,
    GalleryModule.forRoot(),
    NgxPaginationModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    AgGridModule.withComponents([]),
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    UserIdleModule.forRoot({ idle: 1650, timeout: 60, ping: 60 }),
    HttpClientModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    NgxFileDropModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({})
  ],
  declarations: [AppComponent, APP_CONTAINERS, ErrorComponent, LoginComponent],
  providers: [
    ProductService,
    ConfigService,
    DataService,
    LoginService,
    PuntoventaService,
    RolesService,
    VentasService,
    PromocionService,
    CartService,
    ClientService,
    UserService,
    SchedulesvisitsService,
    MaterialsService,
    PaginServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
