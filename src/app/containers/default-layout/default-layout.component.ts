import { Component, OnInit } from '@angular/core';
import { navItems, navItems2 } from '../../_nav';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import CryptoJS from "crypto-js";
import { decode, encode } from 'punycode';
import { SchedulesvisitsService } from '../../services/schedulesvisits.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {

  public navItems = navItems;
  public navItems2 = navItems2;
  public visits = [];
  public visitsPen = [];
  public sidebarMinimized = true;
  public element: HTMLElement;
  public userSession:any;
  public typeUserSession:any;

  constructor(
    public loginService:LoginService,
    public schedulesvisitsService: SchedulesvisitsService,
    private router: Router
  ){ }

  ngOnInit(){
    if(localStorage.length){
      // Asignar nombre de usuario desde el storage
      var user = localStorage.getItem('sessionName')
      var bytes  = CryptoJS.AES.decrypt(user, 'sigma7');
      var users = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      this.userSession = users

      // veficiar usuario en sesion durante evento f5
      var codes = localStorage.getItem('sessionCode')
      var bytes  = CryptoJS.AES.decrypt(codes, 'sigma7');
      var code = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      let object = { sessionCode:code }
      this.reloadPage(object);
    }else{
      this.router.navigate(['/login']);
    }
  }

  closeSession(){
      //  cerrar sesion DB
      var session = localStorage.getItem('sessionId')
      var bytes  = CryptoJS.AES.decrypt(session, 'sigma7');
      var sessionid = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      let obj = { sessionId: sessionid};
      this.loginService.closeSession(obj).subscribe( (res:any) => {
        if(res){
            // 200
            res.status
          }else{
            // 400
            res.status
          }
        })

        // borrar sesion localstorage del navegador
        localStorage.removeItem('sessionId')
        localStorage.removeItem('sessionName')
        localStorage.removeItem('sessionCode')
        localStorage.removeItem('sessionTypeUser')
        localStorage.removeItem('token')

  }

  getVisitsDay(){
    this.schedulesvisitsService.getVisitsDay().subscribe((res:any) =>{
      this.visits = res;
    })
  }

  getVisitsPen(){
    this.schedulesvisitsService.getWebVisitsPen().subscribe((res:any) =>{
      this.visitsPen = res;
    })
  }

  reloadPage(object:any){
    if(object){
      var types = localStorage.getItem('sessionTypeUser')
      var bytes  = CryptoJS.AES.decrypt(types, 'sigma7');
      var type = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      this.loginService.reloadSession(object).subscribe( async (res:any)=>{
        if(res.status){
          this.typeUserSession = type
          await this.getVisitsDay();
          this.getVisitsPen();
        }else{
          this.router.navigate(['/login']);
        }
      })
    }else{
      this.router.navigate(['/login']);
    }
  }

}
