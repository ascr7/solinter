import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Alert Component
import { AlertModule } from "ngx-bootstrap/alert";

// Modal Component
import { ModalModule } from "ngx-bootstrap/modal";
import { UsersComponent } from "./users.component";
import { TabsModule } from "ngx-bootstrap/tabs";

import { UsersRoutingModule } from "./users-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxLoadingModule } from "ngx-loading";
import { ListUsersComponent } from "./list-users/list-users.component";
import { DetailUserComponent } from "./detail-user/detail-user.component";
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    TabsModule,
    UsersRoutingModule,
    AlertModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [UsersComponent, ListUsersComponent, DetailUserComponent]
})
export class UsersModule {}
