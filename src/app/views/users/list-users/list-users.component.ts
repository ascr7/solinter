import { Component, ViewChild } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { UserService } from "../../../services/user.service";
import { TypeuserService } from "../../../services/typeuser.service";
// PROPIEDADES PARA FORMULARIOS
import {
  FormGroup,
  FormBuilder,
  AbstractControl,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
import CryptoJS from "crypto-js";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: "app-list-users",
  templateUrl: "./list-users.component.html",
  styleUrls: ["./list-users.component.scss"]
})
export class ListUsersComponent {
  //  MODAL PARA LA VISTAS
  @ViewChild("myModal", { static: false }) public myModal: ModalDirective;
  @ViewChild("infoModal", { static: false }) public infoModal: ModalDirective;

  // VARIABLES PARA EL FORMULARIO
  public users: any[];
  public usersType: any;
  public showModEdit = false;
  public response: any;
  public alert: any;
  public page: any;
  public p: number = 1;

  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editUsersForm: FormGroup; // NOMBRE DEL FOMULARIO
  public deleteUsersForm: FormGroup;
  public id: AbstractControl;
  public name: AbstractControl;
  public phone: AbstractControl;
  public address: AbstractControl;
  public email: AbstractControl;
  public userName: AbstractControl;
  public userType: AbstractControl;
  public identification: AbstractControl;
  public loading: boolean;

  constructor(
    public userService: UserService,
    public typeUserService: TypeuserService,
    private formBuilder: FormBuilder,
    private router: Router,
    public demoService: NgxUiLoaderService
  ) {
    // VALIDACIONES BACKEND
    this.editUsersForm = this.formBuilder.group({
      id: [""],
      name: ["", Validators.required],
      identification: ["", Validators.required],
      phone: ["", Validators.required],
      address: ["", Validators.required],
      email: ["", Validators.required],
      userName: ["", Validators.required],
      userType: [""],
      userPass: ["", Validators.required]
    });

    this.deleteUsersForm = this.formBuilder.group({
      iduser: [""]
    });

    // VALIDACIONES FRONTEND
    this.name = this.editUsersForm.controls["name"];
    this.identification = this.editUsersForm.controls["identification"];
    this.phone = this.editUsersForm.controls["phone"];
    this.address = this.editUsersForm.controls["address"];
    this.email = this.editUsersForm.controls["email"];
    this.userName = this.editUsersForm.controls["userName"];
  }

  //  INICIALIZAR VARIABLES
  ngOnInit() {
    const privileges = 1 || 2;
    // capturar tipo usuario del navegador localstorage y desencriptar
    var bytes = CryptoJS.AES.decrypt(
      localStorage.getItem("sessionTypeUser"),
      "sigma7"
    );
    var sessionTypeUser = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    if (privileges == sessionTypeUser) {
      this.getUsers();
    } else {
      this.router.navigate(["/login"]);
    }
    this.getTypeUsers();
  }

  // MOSTRAR CONTENIDO
  getUsers() {
    //  Open loading
    this.loading = true;
    this.userService.getUsers().subscribe((res: any) => {
      //  console.log(res)
      this.users = res;
      //  Close loading
      this.loading = false;
    });
  }

  // MOSTRAR USUARIOS
  getTypeUsers() {
    this.typeUserService.getTypeUsers().subscribe((res: any[]) => {
      // console.log(res)
      this.usersType = res;
    });
  }

  clearForm() {
    this.editUsersForm.reset();
  }

  // AGREGAR CONTENIDO
  registerUsers() {
    if (this.editUsersForm.value.id) {
      // UPDATE USERS
      // IGUALAR NOMBRES DEL FORMULARIO CON NOMBRES DEL BACKEND
      const users = {
        idUser: this.editUsersForm.value.id,
        UserID: this.editUsersForm.value.identification,
        UserName: this.editUsersForm.value.name,
        UserAddress: this.editUsersForm.value.address,
        UserPhone: this.editUsersForm.value.phone,
        UserEmail: this.editUsersForm.value.email,
        userNam: this.editUsersForm.value.userName,
        UserTypeName: this.editUsersForm.value.userType,
        UserPassword: this.editUsersForm.value.userPass
      };

      // ENVIAR DATOS(users) AL BACKEND PARA GUARDAR
      this.userService.putUser(users).subscribe((res: any) => {
        this.getUsers();
        if (res.status) {
          this.response = "Usuario actualizado!";
          this.alert = "alert alert-info";
        }
      });
    } else {
      // CREATE USERS
      const newUser = {
        UserID: this.editUsersForm.value.identification,
        UserName: this.editUsersForm.value.name,
        UserAddress: this.editUsersForm.value.address,
        UserPhone: this.editUsersForm.value.phone,
        UserEmail: this.editUsersForm.value.email,
        userNam: this.editUsersForm.value.userName,
        UserTypeName: this.editUsersForm.value.userType,
        UserPassword: this.editUsersForm.value.userPass
      };

      this.userService.registerUsers(newUser).subscribe((res: any) => {
        this.getUsers();
        this.response = "El usuario se registro con exito!";
        this.alert = "alert alert-success";
      });
    }
  }

  // EDITAR TABLAS
  editUsers(users: any) {
    // ASIGNAR VALORES EN EL FORMULARIO MODAL
    this.editUsersForm.setValue({
      id: users.iduser,
      identification: users.user_id,
      name: users.user_name,
      phone: users.user_phone,
      address: users.user_address,
      email: users.user_email,
      userName: users.username,
      userType: users.user_usertype,
      userPass: users.password
      // userPass: (sha1.create()).update(users.password),
    });
    this.showModEdit = true;
  }

  deleteUser(user: any) {
    this.deleteUsersForm.setValue({
      iduser: user.iduser
    });
    this.showModEdit = true;
  }

  deleteUsers() {
    // ENVIAR DATOS(user) AL BACKEND PARA GUARDAR
    this.userService
      .deleteUser(this.deleteUsersForm.value.iduser)
      .subscribe((res: any) => {
        this.getUsers();
        if (res.status) {
          this.response = "El usuarios se elimino de forma permanente!";
          this.alert = "alert alert-danger";
        }
      });
  }

  buscar(words: any) {
    // console.log(words);
    let obj = { letra: words };
    this.userService.findUsers(obj).subscribe((res: any) => {
      this.users = res.users;
    });
  }
}
