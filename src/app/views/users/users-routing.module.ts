import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ListUsersComponent } from "./list-users/list-users.component";
import { DetailUserComponent } from "./detail-user/detail-user.component";

const routes: Routes = [
  {
    path: "usuario/:id",
    component: DetailUserComponent,
    data: {
      title: "Gestion de Usuarios"
    }
  },
  {
    path: "",
    component: ListUsersComponent,
    data: {
      title: "Gestion de Usuarios"
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
