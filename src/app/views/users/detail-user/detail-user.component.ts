import { Component, OnInit } from "@angular/core";
import { UserService } from "../../../services/user.service";
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ActivatedRoute } from "@angular/router";
import CryptoJS from "crypto-js";
import { SchedulesvisitsService } from "../../../services/schedulesvisits.service";

@Component({
  selector: "app-detail-user",
  templateUrl: "./detail-user.component.html",
  styleUrls: ["./detail-user.component.scss"]
})
export class DetailUserComponent implements OnInit {
  public user: any[];
  public loading: boolean;
  public done: boolean;
  public disabled = true;
  public edit = false;
  public visitsPen: any;
  public visitsAct: any;
  public visitsActLength: number;
  public visitsPenLength: number;
  public userForm: FormGroup;
  public id: AbstractControl;
  public name: AbstractControl;
  public phone: AbstractControl;
  public address: AbstractControl;
  public email: AbstractControl;
  public userName: AbstractControl;
  public userPass: AbstractControl;
  public identification: AbstractControl;

  constructor(
    public userService: UserService,
    public scheduleService: SchedulesvisitsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public demoService: NgxUiLoaderService
  ) {
    this.userForm = this.formBuilder.group({
      id: ["", Validators.required],
      name: ["", Validators.required],
      identification: ["", Validators.required],
      phone: ["", Validators.required],
      address: ["", Validators.required],
      email: ["", Validators.required],
      userName: ["", Validators.required],
      userPass: ["", Validators.required]
    });

    this.name = this.userForm.controls["name"];
    this.identification = this.userForm.controls["identification"];
    this.phone = this.userForm.controls["phone"];
    this.address = this.userForm.controls["address"];
    this.email = this.userForm.controls["email"];
    this.userName = this.userForm.controls["userName"];
    this.userPass = this.userForm.controls["userPass"];
  }

  ngOnInit() {
    const privileges = 1 || 2;
    let id = this.route.snapshot.params.id;
    var bytes = CryptoJS.AES.decrypt(
      localStorage.getItem("sessionTypeUser"),
      "sigma7"
    );
    var sessionTypeUser = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    if (privileges == sessionTypeUser) {
      this.getUser(id);
    } else {
      this.router.navigate(["/login"]);
    }
  }

  getUser(id) {
    this.done = true;
    this.userService.getUser(id).subscribe((res: any) => {
      let user = res;
      this.user = user;
      this.userForm.get("id").disable();
      this.userForm.get("name").disable();
      this.userForm.get("identification").disable();
      this.userForm.get("phone").disable();
      this.userForm.get("address").disable();
      this.userForm.get("email").disable();
      this.userForm.get("userName").disable();
      this.userForm.get("userPass").disable();
      for (let i = 0; i < user.length; i++) {
        const e = user[i];
        this.userForm.setValue({
          id: e.iduser,
          name: e.user_name,
          identification: e.user_id,
          phone: e.user_phone,
          address: e.user_address,
          email: e.user_email,
          userName: e.username,
          userPass: e.password
        });
        this.getVisitsPen(e.iduser);
        this.getVisitsAct(e);
      }
    });
    this.done = true;
  }

  getVisitsPen(iduser: string) {
    this.scheduleService.getVisitsPen(iduser).subscribe((res: any) => {
      this.visitsPen = res;
      this.visitsPenLength = res.length;
    });
  }

  getVisitsAct(e: any) {
    let obj = {
      iduser: e.iduser,
      typeUser: e.idusertype
    };
    this.scheduleService.getVisits(obj).subscribe((res: any) => {
      this.visitsAct = res;
      this.visitsActLength = res.length;
    });
  }

  onSubmit() {
    console.log(this.user);
  }

  EditChange() {
    this.edit = !this.edit;
    this.userForm.get("id").enable();
    this.userForm.get("name").enable();
    this.userForm.get("identification").enable();
    this.userForm.get("phone").enable();
    this.userForm.get("address").enable();
    this.userForm.get("email").enable();
    this.userForm.get("userName").enable();
    this.userForm.get("userPass").enable();
  }
}
