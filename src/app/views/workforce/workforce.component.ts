import { Component, ViewChild } from '@angular/core';
import { ModalDirective} from 'ngx-bootstrap/modal';
import { WorkforceService }  from "../../services/workforce.service";
import { CategoryService } from '../../services/category.service';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// PROPIEDADES PARA FORMULARIOS
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';

@Component({
  selector: 'workforce',
  templateUrl: 'workforce.component.html',
})
export class WorkforceComponent {

  // MODAL PARA LAS TABLAS
  @ViewChild('myModal', { static: false }) public myModal: ModalDirective;
  @ViewChild('infoModal', { static: false }) public infoModal: ModalDirective;

  // @Input('data') workforce: string[] = [];

  // INICIALIZAR VARIABLES
  public workforce:any;
  public page:any;
  public categories:any;
  public showModEdit = false;
  public response:any;
  public alert:any;
  p: number = 1;

  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editWorkforcesForm: FormGroup // NOMBRE DEL FOMULARIO
  public deleteWorkforcesForm: FormGroup
  public id: AbstractControl
  public code: AbstractControl
  public name: AbstractControl
  public price: AbstractControl
  public category: AbstractControl
  public unit: AbstractControl
  public files: NgxFileDropEntry[] = [];
  public path: any
  public loading = false;
  public showWorkF = false;


  constructor (
    public workforceService:  WorkforceService,
    public categoryService: CategoryService,
    private http: HttpClient,
    private formBuilder: FormBuilder

    ){

      // VALIDACIONES BACKEND
      this.editWorkforcesForm = this.formBuilder.group({
        id: ['' ],
        code: ['', Validators.required ],
        name: ['', Validators.required ],
        price: ['', Validators.required ],
        unit: ['', Validators.required ],
        category: ['', Validators.required ],
      })

      this.deleteWorkforcesForm = this.formBuilder.group({
        idworkf: ['']
      })

    // VALIDACIONES FRONTEND
    this.code = this.editWorkforcesForm.controls['code']
    this.name = this.editWorkforcesForm.controls['name']
    this.price = this.editWorkforcesForm.controls['price']
    this.unit = this.editWorkforcesForm.controls['unit']
    this.category = this.editWorkforcesForm.controls['category']

    }

    // INICIALIZAR VARIABLES A USAR
    ngOnInit(){
      this.getWorkforces(),
      this.getCategories()
    }
    // MOSTRAR MATERIALES
    getWorkforces(){
      this.loading = true
      this.workforceService.getWorkforces().subscribe((res:any) =>{
        this.workforce = res;
        this.showWorkF = true
        this.loading = false
      })
    }
    // MOSTRAR CATEGORIAS DE MATERIALES
    getCategories(){
      this.categoryService.getCategories().subscribe((res:any) =>{
        // console.log(res)
        this.categories = res.filter(category => category.category_type == 4)
      })
    }
    clearForm(){
      this.editWorkforcesForm.reset();
    }
    dropped(files: NgxFileDropEntry[]) {
      this.files = files;
      for (const droppedFile of files) {
        // Is it a file?
        if (droppedFile.fileEntry.isFile) {
          const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
          fileEntry.file((file: File) => {
            // You could upload it like this:
            const formData = new FormData()
            formData.append('file', file, droppedFile.relativePath)
            // Headers
            const headers = new HttpHeaders({
              'security-token': 'mytoken'
            })
            this.http.post('http://198.27.127.186:8000/upload', formData, { headers: headers, responseType: 'text' })
            .subscribe(data => {
              this.path = {
                name: data.replace(" ",""),
                price: "sig_workforce"
              }
            })
          });
        } else {
          // It was a directory (empty directories are added, otherwise only files)
          const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
          console.log(droppedFile.relativePath, fileEntry);
        }
      }
    }
    chargeFile(){
      this.loading = true
      this.http.post('http://198.27.127.186:8000/upload/create', this.path).subscribe((res:any) => {
        if (res.status) {
          this.getWorkforces()
        }
      })
    }
    // AGREGAR CONTENIDO
    registerWorkforce()
    {
      if(this.editWorkforcesForm.value.id)
      {
        // UPDATE WORKFORCE
        const workforce={
          idworkf: this.editWorkforcesForm.value.id,
          code: this.editWorkforcesForm.value.code,
          name: this.editWorkforcesForm.value.name,
          price: this.editWorkforcesForm.value.price,
          unit: this.editWorkforcesForm.value.unit,
          category: this.editWorkforcesForm.value.category
        }
        this.workforceService.putWorkforce(workforce).subscribe( (res:any) => {
          this.getWorkforces()
          if (res.status) {
              this.response = 'La maquinaria fue actualizado!'
              this.alert = 'alert alert-info'
            }
          })
        }
      else
      {
        // CREATE WORKFORCE
        const newWorkforce = {
          code: this.editWorkforcesForm.value.code,
          name: this.editWorkforcesForm.value.name,
          price: this.editWorkforcesForm.value.price,
          unit: this.editWorkforcesForm.value.unit,
          category: this.editWorkforcesForm.value.category
          };
            this.workforceService.registerWorkforces(newWorkforce).subscribe( (res:any) => {
            this.getWorkforces()
            this.response = 'La maquinaria se registro con exito!'
            this.alert = 'alert alert-success'
        })
      }
    }
    // EDITAR TABLAS
    editWorkforces(workforce:any){
      this.editWorkforcesForm.setValue({
          id:  workforce.idworkf,
          name: workforce.name,
          code: workforce.code,
          price:workforce.price,
          unit:workforce.unit,
          category:workforce.id_category,
        })
      this.showModEdit = true
    }
    deleteWorkforce(workforce:any){
      this.deleteWorkforcesForm.setValue({
          idworkf:workforce.idworkf
      })
      this.showModEdit = true
    }
    deleteWorkforces(){
      // ENVIAR DATOS(this.workforce) AL BACKEND PARA GUARDAR
      this.workforceService.deleteWorkforce(this.deleteWorkforcesForm.value.idworkf).subscribe((res:any)=>{
          this.getWorkforces()
          if(res.status){
            this.response = 'La maquinaria se elimino de forma permanente!'
            this.alert = 'alert alert-danger'
          }
      })
    }

    // BUSQUEDA EN DATATABLE
    find(words:any){
      // console.log(words);
      let obj = { letra: words};
      this.workforceService.findWorkforces(obj).subscribe((res:any) =>{
          this.workforce = res.workforces
      })
    }

}

