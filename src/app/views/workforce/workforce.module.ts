import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { WorkforceComponent } from './workforce.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { WorkforceRoutingModule } from './workforce-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxFileDropModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    WorkforceRoutingModule,
    NgxLoadingModule.forRoot({}),
    AlertModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [
    WorkforceComponent ]
})
export class WorkforcesModule { }
