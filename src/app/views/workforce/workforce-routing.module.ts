import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkforceComponent } from './workforce.component';
import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  {
    path: '',
    component: WorkforceComponent,
    data: {
      title: 'Mano de obra'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    DataTablesModule.forRoot(),
  ],
  exports: [RouterModule]
  // declarations: [WorkforceComponent]
})
export class WorkforceRoutingModule {}

