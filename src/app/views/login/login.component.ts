import { Component, ViewChild } from '@angular/core';
import { ModalDirective} from 'ngx-bootstrap/modal';
import { LoginService } from '../../services/login.service';
import { Router} from '@angular/router';
import CryptoJS from "crypto-js";
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from "angular-web-storage";
// PROPIEDADES PARA FORMULARIOS
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { by } from 'protractor';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  // MODAL PARA LAS TABLAS
  @ViewChild('myModal', { static: false }) public myModal: ModalDirective;
  @ViewChild('infoModal', { static: false }) public infoModal: ModalDirective;

  public users:any;

  public loginForm: FormGroup
  public username: AbstractControl
  public password: AbstractControl
  public response:any;
  public alert:any;

  constructor(
    public loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,
    public local: LocalStorageService,
    public session: SessionStorageService
  ){

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required ],
      password: ['', Validators.required ],
  })

  // VALIDACIONES FRONTEND
  this.username = this.loginForm.controls['username']
  this.password = this.loginForm.controls['password']

  }

  startSession(){
      this.loginService.loginUser(this.loginForm.value).subscribe( (res:any) => {
        if (res.status)
          {
              // Crear string y Guardar en localStorage
              var sessionCode = CryptoJS.AES.encrypt(JSON.stringify(res.sessionCode), 'sigma7').toString();
              localStorage.setItem('sessionCode', sessionCode);
              var sessionId = CryptoJS.AES.encrypt(JSON.stringify(res.userId), 'sigma7').toString();
              localStorage.setItem('sessionId', sessionId);
              var userName = CryptoJS.AES.encrypt(JSON.stringify(res.userName), 'sigma7').toString();
              localStorage.setItem('sessionName', userName);
              var typeUser = CryptoJS.AES.encrypt(JSON.stringify(res.typeUser), 'sigma7').toString();
              localStorage.setItem('sessionTypeUser', typeUser);
              var token = CryptoJS.AES.encrypt(JSON.stringify(res.token), 'sigma7').toString();
              localStorage.setItem('token', token);
              this.router.navigate(['/inicio']);
            }else{
                  this.router.navigate(['/login']);
                  this.response = ' Usuario o contraseña Incorrecto!'
                  this.alert = 'alert alert-danger'
                }
      })

  }


}
