import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListclientsComponent } from './listclients/listclients.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  { path: '',  component: ListclientsComponent, data: { animation: 'heroes' } },
  { path: 'empresa/:id', component: DetailComponent, data: { animation: 'hero' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule {}
