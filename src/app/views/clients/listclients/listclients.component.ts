import { Component, ViewChild } from '@angular/core';
import { ModalDirective} from 'ngx-bootstrap/modal';
import { ClientService }  from "../../../services/client.service";
// PROPIEDADES PARA FORMULARIOS
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import CryptoJS from "crypto-js";


@Component({
  selector: 'listclients',
  templateUrl: 'listclients.component.html',
  // changeDetection: ChangeDetectionStrategy.Default
})
export class ListclientsComponent {

  //  MODAL PARA LA VISTAS
  @ViewChild('myModal', { static: false }) public myModal: ModalDirective;
  @ViewChild('infoModal', { static: false }) public infoModal: ModalDirective;

  // VARIABLES PARA EL FORMULARIO
  public clients:any;
  public page:any;
  public showModEdit = false;
  public response:any;
  public alert:any;

  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editClientsForm: FormGroup // NOMBRE DEL FOMULARIO
  public deleteClientsForm: FormGroup 
  public address: AbstractControl
  public contact: AbstractControl
  public email: AbstractControl
  public name: AbstractControl
  public nit: AbstractControl
  public phone: AbstractControl
  public clientType: AbstractControl
  public loading: boolean;

  constructor(
    public clientService: ClientService,
    private formBuilder: FormBuilder,
    private router: Router,
    
    ){
    
      // VALIDACIONES BACKEND
      this.editClientsForm = this.formBuilder.group({
        id: ['' ],
        nit: ['', Validators.required ],
        name: ['', Validators.required ],
        address: ['', Validators.required ],
        email: ['', Validators.required ],
        phone: ['', Validators.required ],
        contact: ['', Validators.required ],
        clientType: ['', Validators.required ],
      })

      this.deleteClientsForm = this.formBuilder.group({
        idclient: ['']
      })        

    // VALIDACIONES FRONTEND
      this.nit = this.editClientsForm.controls['nit']
      this.name = this.editClientsForm.controls['name']
      this.address = this.editClientsForm.controls['materialName']
      this.email = this.editClientsForm.controls['email']
      this.phone = this.editClientsForm.controls['phone']
      this.contact = this.editClientsForm.controls['contact']
      this.clientType = this.editClientsForm.controls['clientType']
    
  }
  
  //  INICIALIZAR VARIABLES
  ngOnInit(){
    // validar privilegios segun type usuario
    const privileges = 1 || 2
    // capturar tipo usuario del navegador localstorage y desencriptar
    var bytes  = CryptoJS.AES.decrypt(localStorage.getItem('sessionTypeUser'), 'sigma7');
    var sessionTypeUser = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    if(privileges == sessionTypeUser){
        this.getClients()
      }else{
          this.router.navigate(['/login']);
        }
  }

  // MOSTRAR CONTENIDO
  getClients(){
    //  open loading
    this.loading = true
    this.clientService.getClients().subscribe((res:any) =>{
        this.clients = res,
        //  stop loading
        this.loading = false
    })
  }

  clearForm(){
    this.editClientsForm.reset();
  }

  // AGREGAR CONTENIDO
  registerClients()
  {
    if(this.editClientsForm.value.id)
    { 
      // UPDATE MATERIALS
      // IGUALAR NOMBRES DEL FORMULARIO CON NOMBRES DEL BACKEND
      const clients={
          id: this.editClientsForm.value.id,
          nit: this.editClientsForm.value.nit,
          name: this.editClientsForm.value.name,
          address: this.editClientsForm.value.address,
          phone:  this.editClientsForm.value.phone,
          email: this.editClientsForm.value.email,
          contact: this.editClientsForm.value.contact,
          typeclient: this.editClientsForm.value.clientType,
      }
      // ENVIAR DATOS(clients) AL BACKEND PARA GUARDAR
      this.clientService.putClient(clients).subscribe( (res:any) => {
          this.getClients()
          if (res.status) {
            this.response = 'Empresa fue actualizada!'
            this.alert = 'alert alert-info'
          }
        })
    }
    else
    {   
      // CREATE CLIENTS  
      const newClient = {
        nit: this.editClientsForm.value.nit,
        name: this.editClientsForm.value.name,
        address: this.editClientsForm.value.address,
        phone:this.editClientsForm.value.phone,
        email: this.editClientsForm.value.email,
        contact: this.editClientsForm.value.contact,
        imageclient:'',
        typeclient: 1,
        status : 1,
        imagetype:''
      };

      this.clientService.registerClient(newClient).subscribe( (res:any) => {
          this.getClients()
          this.response = 'La empresa se registro con exito!'
          this.alert = 'alert alert-success'
      })

    }
  }

  // GARGAR CAMPOS A EDITAR 
  editClients(clients:any){
    // ASIGNAR VALORES EN EL FORMULARIO MODAL 
      this.editClientsForm.setValue({ 
        id:  clients.idclient,
        nit:  clients.client_nit,
        name:  clients.client_name,
        address: clients.client_address,
        email:  clients.client_email,
        phone: clients.client_phone,
        contact: clients.client_contact,
        clientType: clients.client_phone
      })
      this.showModEdit = true     
  }

  deleteClient(clients:any){
    this.deleteClientsForm.setValue({ 
        idclient:clients.idclient
    })
    this.showModEdit = true
  }

  deleteClients(){
  // ENVIAR DATOS(clients) AL BACKEND PARA GUARDAR
  this.clientService.deleteClient(this.deleteClientsForm.value.idclient).subscribe((res:any)=>{
      this.getClients()
      if(res.status){
        this.response = 'La empresa se elimino de forma permanente!'
        this.alert = 'alert alert-danger'
      }
    })
  }

  find(words:any){
      // console.log(words);
      let obj = { letra: words};
      this.clientService.findClients(obj).subscribe((res:any) =>{
          this.clients = res.clients
      })    
  }

}
