import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

// Components
import { ListclientsComponent } from "./listclients/listclients.component";
import { DetailComponent } from "./detail/detail.component";

//Routing
import { ClientsRoutingModule } from './clients-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    ClientsRoutingModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
  ],
  declarations: [
    ListclientsComponent,
    DetailComponent ]
})
export class ClientsModule { }
