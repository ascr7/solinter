import { Component, OnInit } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {



  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  constructor( public DF:DefaultLayoutComponent, public userIdle:UserIdleService){}

  ngOnInit() {
    this.userIdle.startWatching();
    this.userIdle.onTimerStart().subscribe(count => {
      // console.log(count);
      if (count == 20) {
        this.stop()
      }
    });
  }

  stop() {
    this.userIdle.stopTimer();
    this.userIdle.stopWatching();
    this.userIdle.resetTimer();
    this.DF.closeSession()
    location.reload()
  }
}
