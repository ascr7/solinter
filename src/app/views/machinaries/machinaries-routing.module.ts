import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MachinariesComponent } from './machinaries.component';
import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  {
    path: '',
    component: MachinariesComponent,
    data: {
      title: 'Maquinarias'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    DataTablesModule.forRoot(),
  ],
  exports: [RouterModule]
  // declarations: [MachinariesComponent]
})
export class MachinariesRoutingModule {}
