import { Component, ViewChild, ChangeDetectionStrategy } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { MachinariesService } from "../../services/machinaries.service";
import { CategoryService } from "../../services/category.service";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry
} from "ngx-file-drop";
import { HttpClient, HttpHeaders } from "@angular/common/http";
// PROPIEDADES PARA FORMULARIOS
import {
  FormGroup,
  FormBuilder,
  AbstractControl,
  Validators
} from "@angular/forms";

@Component({
  selector: "machinaries",
  templateUrl: "machinaries.component.html",
  changeDetection: ChangeDetectionStrategy.Default
})
export class MachinariesComponent {
  // MODAL PARA LAS TABLAS
  @ViewChild("myModal", { static: false }) public myModal: ModalDirective;
  @ViewChild("infoModal", { static: false }) public infoModal: ModalDirective;

  // @Input('data') machinaries: string[] = [];

  // INICIALIZAR VARIABLES
  public machinaries: any;
  public page: any;
  public categories: any;
  public showModEdit = false;
  public response: any;
  public alert: any;
  p: number = 1;

  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editMachinariesForm: FormGroup; // NOMBRE DEL FOMULARIO
  public deleteMachinariesForm: FormGroup;
  public id: AbstractControl;
  public code: AbstractControl;
  public name: AbstractControl;
  public price: AbstractControl;
  public category: AbstractControl;
  public unit: AbstractControl;
  public files: NgxFileDropEntry[] = [];
  public path: any;
  public loading = false;
  public showMachinary = false;

  private gridApi;
  private gridcolumnApi;
  private columnDefs;
  private sortingOrder;

  constructor(
    public machinariesService: MachinariesService,
    public categoryService: CategoryService,
    private formBuilder: FormBuilder,
    private http: HttpClient
  ) {
    this.columnDefs = [
      {
        headerName: "Codigo",
        width: 150,
        sortingOrder: ["asc", "desc"],
        field: "code"
      },
      {
        headerName: "Nombre",
        width: 50,
        sortingOrder: ["asc", "desc"],
        field: "name"
      }
    ];
    // VALIDACIONES BACKEND
    this.editMachinariesForm = this.formBuilder.group({
      id: [""],
      code: ["", Validators.required],
      name: ["", Validators.required],
      price: ["", Validators.required],
      unit: ["", Validators.required],
      category: ["", Validators.required]
    });

    this.deleteMachinariesForm = this.formBuilder.group({
      idmaterial: [""]
    });

    // VALIDACIONES FRONTEND
    this.code = this.editMachinariesForm.controls["code"];
    this.name = this.editMachinariesForm.controls["name"];
    this.unit = this.editMachinariesForm.controls["unit"];
    this.price = this.editMachinariesForm.controls["price"];
    this.category = this.editMachinariesForm.controls["category"];
  }

  // INICIALIZAR VARIABLES A USAR
  ngOnInit() {
    this.getMachinaries(), this.getCategories();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridcolumnApi = params.columnApi;
    let dataValue = [{ code: 2000, name: "Apple Watch" }];
    params.api.setRowData(dataValue);
  }
  // MOSTRAR MATERIALES
  getMachinaries() {
    this.loading = true;
    this.machinariesService.getMachinaries().subscribe((res: any) => {
      this.machinaries = res;
      this.showMachinary = true;
      this.loading = false;
    });
  }
  // MOSTRAR CATEGORIAS DE MATERIALES
  getCategories() {
    this.categoryService.getCategories().subscribe((res: any) => {
      this.categories = res.filter(category => category.category_type == 3);
    });
  }
  clearForm() {
    this.editMachinariesForm.reset();
  }
  dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          // You could upload it like this:
          const formData = new FormData();
          formData.append("file", file, droppedFile.relativePath);
          // Headers
          const headers = new HttpHeaders({
            "security-token": "mytoken"
          });
          this.http
            .post("http://198.27.127.186:8000/upload", formData, {
              headers: headers,
              responseType: "text"
            })
            .subscribe(data => {
              this.path = {
                name: data.replace(" ", ""),
                price: "sig_machinaries"
              };
            });
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }
  chargeFile() {
    this.loading = true;
    console.log(this.path);

    this.http
      .post("http://198.27.127.186:8000/upload/create", this.path)
      .subscribe((res: any) => {
        if (res.status) {
          this.loading = false;
          this.getMachinaries();
        }
      });
  }
  // AGREGAR CONTENIDO
  registerMachinaries() {
    if (this.editMachinariesForm.value.id) {
      // UPDATE MACHINARY
      const machinaries = {
        idmaterial: this.editMachinariesForm.value.id,
        code: this.editMachinariesForm.value.code,
        name: this.editMachinariesForm.value.name,
        price: this.editMachinariesForm.value.price,
        unit: this.editMachinariesForm.value.unit,
        category: this.editMachinariesForm.value.category
      };
      this.machinariesService
        .putMachinary(machinaries)
        .subscribe((res: any) => {
          this.getMachinaries();
          if (res.status) {
            this.response = "La maquinaria fue actualizado!";
            this.alert = "alert alert-info";
          }
        });
    } else {
      // CREATE MACHINARY
      const newMaterial = {
        code: this.editMachinariesForm.value.code,
        name: this.editMachinariesForm.value.name,
        price: this.editMachinariesForm.value.price,
        unit: this.editMachinariesForm.value.unit,
        category: this.editMachinariesForm.value.category
      };
      this.machinariesService
        .registerMachinaries(newMaterial)
        .subscribe((res: any) => {
          this.getMachinaries();
          this.response = "La maquinaria se registro con exito!";
          this.alert = "alert alert-success";
        });
    }
  }
  // EDITAR TABLAS
  editMachinaries(machinaries: any) {
    this.editMachinariesForm.setValue({
      id: machinaries.idmaterial,
      name: machinaries.name,
      code: machinaries.code,
      price: machinaries.price,
      unit: machinaries.unit,
      category: machinaries.idcategory
    });
    this.showModEdit = true;
  }
  deleteMachinary(machinaries: any) {
    this.deleteMachinariesForm.setValue({
      idmaterial: machinaries.idmaterial
    });
    this.showModEdit = true;
  }
  deleteMachinaries() {
    // ENVIAR DATOS(this.machinaries) AL BACKEND PARA GUARDAR
    this.machinariesService
      .deleteMachinary(this.deleteMachinariesForm.value.idmaterial)
      .subscribe((res: any) => {
        this.getMachinaries();
        if (res.status) {
          this.response = "La maquinaria se elimino de forma permanente!";
          this.alert = "alert alert-danger";
        }
      });
  }

  // BUSQUEDA EN DATATABLE
  find(words: any) {
    let obj = { letra: words };
    this.machinariesService.findMachinary(obj).subscribe((res: any) => {
      this.machinaries = res.machinaries;
    });
  }
}
