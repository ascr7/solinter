import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { MachinariesComponent } from './machinaries.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { MachinariesRoutingModule } from './machinaries-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxFileDropModule,
    ReactiveFormsModule,
    MachinariesRoutingModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({}),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
  ],
  declarations: [
    MachinariesComponent ]
})
export class MachinariesModule { }
