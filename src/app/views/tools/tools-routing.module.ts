import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ToolsComponent } from './tools.component';
import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  {
    path: '',
    component: ToolsComponent,
    data: {
      title: 'Herramientas'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    DataTablesModule.forRoot(),
  ],
  exports: [RouterModule]
  // declarations: [ToolsComponent]
})
export class ToolsRoutingModule {}
