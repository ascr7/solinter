import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToolsComponent } from './tools.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ToolsRoutingModule } from './tools-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    NgxFileDropModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    ToolsRoutingModule,
    AlertModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [
    ToolsComponent ]
})
export class ToolsModule { }
