import { Component, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { ModalDirective} from 'ngx-bootstrap/modal';
import { ToolsService }  from "../../services/tools.service";
import { CategoryService } from '../../services/category.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
// PROPIEDADES PARA FORMULARIOS
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';

@Component({
  selector: 'tools',
  templateUrl: 'tools.component.html',
})
export class ToolsComponent {

  // MODAL PARA LAS TABLAS
  @ViewChild('myModal', { static: false }) public myModal: ModalDirective;
  @ViewChild('infoModal', { static: false }) public infoModal: ModalDirective;

  // @Input('data') tools: string[] = [];

  // INICIALIZAR VARIABLES
  public tools:any;
  public page:any;
  public categories:any;
  public showModEdit = false;
  public response:any;
  public alert:any;
  public title_modal = "Registrar herramienta"

  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editToolsForm: FormGroup // NOMBRE DEL FOMULARIO
  public deleteToolsForm: FormGroup
  public id: AbstractControl
  public code: AbstractControl
  public name: AbstractControl
  public price: AbstractControl
  public category: AbstractControl
  public unit: AbstractControl
  public loading = false;
  public showTools = false;
  public files: NgxFileDropEntry[] = [];
  public path: any
  p: number = 1;


  constructor (
    public toolService:  ToolsService,
    public categoryService: CategoryService,
    private http: HttpClient,
    private formBuilder: FormBuilder

    ){

      // VALIDACIONES BACKEND
      this.editToolsForm = this.formBuilder.group({
        id: ['' ],
        code: ['', Validators.required ],
        name: ['', Validators.required ],
        price: ['', Validators.required ],
        unit: ['', Validators.required ],
        category: ['', Validators.required ],
      })

      this.deleteToolsForm = this.formBuilder.group({
        idmaterial: ['']
      })

    // VALIDACIONES FRONTEND
    this.code = this.editToolsForm.controls['code']
    this.name = this.editToolsForm.controls['name']
    this.price = this.editToolsForm.controls['price']
    this.unit = this.editToolsForm.controls['unit']
    this.category = this.editToolsForm.controls['category']

  }

  // INICIALIZAR VARIABLES A USAR
  ngOnInit(){
    this.getTools(),
    this.getCategories()
  }

  // MOSTRAR MATERIALES
  getTools(){
    this.loading = true
    this.toolService.getTools().subscribe((res:any) =>{
      this.tools = res;
      this.showTools = true
      this.loading = false
    })
  }

  // MOSTRAR CATEGORIAS DE MATERIALES
  getCategories(){
    this.categoryService.getCategories().subscribe((res:any) =>{
      this.categories = res.filter(category => category.category_type == 2)
    })
  }

  clearForm(){
    this.editToolsForm.reset();
  }

  dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          // You could upload it like this:
          const formData = new FormData()
          formData.append('file', file, droppedFile.relativePath)
          // Headers
          const headers = new HttpHeaders({
            'security-token': 'mytoken'
          })
          this.http.post('http://198.27.127.186:8000/upload', formData, { headers: headers, responseType: 'text' })
          .subscribe(data => {
            this.path = {
              name: data.replace(" ",""),
              price: "sig_tools"
            }
          })
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  chargeFile(){
    this.loading = true
    this.http.post('http://198.27.127.186:8000/upload/create', this.path).subscribe((res:any) => {
      if (res.status) {
        this.getTools()
      }
    })
  }

  // AGREGAR CONTENIDO
  registerTool()
  {
    if(this.editToolsForm.value.id)
    {
      // UPDATE WORKFORCE
      const tools={
        id: this.editToolsForm.value.id,
        code: this.editToolsForm.value.code,
        name: this.editToolsForm.value.name,
        price: this.editToolsForm.value.price,
        unit: this.editToolsForm.value.unit,
        category: this.editToolsForm.value.category
      }
      this.toolService.putTool(tools).subscribe( (res:any) => {
        this.getTools()
        if (res.status) {
            this.response = 'La herramienta fue actualizada!'
            this.alert = 'alert alert-info'
          }
        })
      }
    else
    {
      // CREATE WORKFORCE
      const newTool = {
        code: this.editToolsForm.value.code,
        name: this.editToolsForm.value.name,
        price: this.editToolsForm.value.price,
        unit: this.editToolsForm.value.unit,
        category: this.editToolsForm.value.category
        };
          this.toolService.registerTools(newTool).subscribe( (res:any) => {
          this.getTools()
          this.response = 'La herramienta se registro con exito!'
          this.alert = 'alert alert-success'
      })
    }
  }

  // EDITAR TABLAS
  editTools(tools:any){
    this.title_modal = "Actualizar herrmienta"
    this.editToolsForm.setValue({
        id:  tools.idmaterial,
        name: tools.name,
        code: tools.code,
        price: tools.price,
        unit: tools.unit,
        category: tools.id_category,
      })
    this.showModEdit = true
  }

  deleteTool(tools:any){
    this.deleteToolsForm.setValue({
        idmaterial:tools.idmaterial
    })
    this.showModEdit = true
  }

  deleteTools(){
    // ENVIAR DATOS(this.tools) AL BACKEND PARA GUARDAR
    this.toolService.deleteTool(this.deleteToolsForm.value.idmaterial).subscribe((res:any)=>{
        this.getTools()
        if(res.status){
          this.getTools()
          this.response = 'La herramienta se elimino de forma permanente!'
          this.alert = 'alert alert-danger'
        }
    })
  }

  // BUSQUEDA EN DATATABLE
  find(words:any){
    // console.log(words);
    let obj = { letra: words};
    this.toolService.findTools(obj).subscribe((res:any) =>{
        this.tools = res.tools
    })
  }

}


