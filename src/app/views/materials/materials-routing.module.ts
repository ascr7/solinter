import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaterialsComponent } from './materials.component';
import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  {
    path: '',
    component: MaterialsComponent,
    data: {
      title: 'Materiales'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    DataTablesModule.forRoot(),
  ],
  exports: [RouterModule]
  // declarations: [MaterialsComponent]
})
export class MaterialsRoutingModule {}
