import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { MaterialsComponent } from './materials.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { MaterialsRoutingModule } from './materials-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    NgxFileDropModule,
    FormsModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    MaterialsRoutingModule,
    AlertModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [
    MaterialsComponent ]
})
export class MaterialsModule { }

