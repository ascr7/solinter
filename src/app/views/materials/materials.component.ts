import {Component, ViewChild } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {MaterialsService }  from "../../services/materials.service";
import { CategoryService } from '../../services/category.service';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
// PROPIEDADES PARA FORMULARIOS
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'materials',
  templateUrl: 'materials.component.html',
})
export class MaterialsComponent {

  // MODAL PARA LAS TABLAS
  @ViewChild('myModal', { static: false }) public myModal: ModalDirective;
  @ViewChild('infoModal', { static: false }) public infoModal: ModalDirective;

  // @Input('data') materials: string[] = [];

  // INICIALIZAR VARIABLES
  public materials:any;
  public page:any;
  public categories:any;
  public showModEdit = false;
  public response:any;
  public alert:any;
  loader:Boolean = false;
  public p: number = 1;
  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editMaterialsForm: FormGroup // NOMBRE DEL FOMULARIO
  public deleteMaterialsForm: FormGroup
  public chargeMaterialsForm: FormGroup
  public id: AbstractControl
  public code: AbstractControl
  public name: AbstractControl
  public file: AbstractControl
  public price: AbstractControl
  public loading = false;
  public showMaterials = false;
  public path: any
  public category: AbstractControl
  public unit: AbstractControl;
  public files: NgxFileDropEntry[] = [];

  constructor(
    public materialsService: MaterialsService,
    public categoryService: CategoryService,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public demoService: NgxUiLoaderService
    ){

      // VALIDACIONES BACKEND
      this.editMaterialsForm = this.formBuilder.group({
        id: ['' ],
        code: ['', Validators.required ],
        name: ['', Validators.required ],
        unit: ['', Validators.required ],
        price: ['', Validators.required ],
        category: ['', Validators.required ],
      })

      this.deleteMaterialsForm = this.formBuilder.group({
        idmaterial: ['']
      })

      this.chargeMaterialsForm = this.formBuilder.group({
        file: ['']
      })

    // VALIDACIONES FRONTEND
    this.code = this.editMaterialsForm.controls['code']
    this.name = this.editMaterialsForm.controls['name']
    this.unit = this.editMaterialsForm.controls['unit']
    this.price = this.editMaterialsForm.controls['price']
    this.category = this.editMaterialsForm.controls['category']

    }
    // INICIALIZAR VARIABLES A USAR
    ngOnInit(){
      this.getMaterials(),
      this.getCategories()
    }
    // MOSTRAR MATERIALES
    getMaterials(){
      this.loading = true
      this.materialsService.getMaterials().subscribe((res:any) =>{
        this.materials = res;
        this.showMaterials = true
        this.loading = false
      })
    }
    // MOSTRAR CATEGORIAS DE MATERIALES
    getCategories(){
      this.categoryService.getCategories().subscribe((res:any) =>{
        // console.log(res)
        this.categories = res.filter(category => category.category_type == 1)
      })
    }
    clearForm(){
      this.editMaterialsForm.reset();
    }

    dropped(files: NgxFileDropEntry[]) {
      this.files = files;
      for (const droppedFile of files) {
        // Is it a file?
        if (droppedFile.fileEntry.isFile) {
          const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
          fileEntry.file((file: File) => {
            // You could upload it like this:
            const formData = new FormData()
            formData.append('file', file, droppedFile.relativePath)
            // Headers
            const headers = new HttpHeaders({
              'security-token': 'mytoken'
            })
            this.http.post('http://198.27.127.186:8000/upload', formData, { headers: headers, responseType: 'text' })
            .subscribe(data => {
              this.path = {
                name: data.replace(" ",""),
                price: "sig_materials"
              }
            })
          });
        } else {
          // It was a directory (empty directories are added, otherwise only files)
          const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
          console.log(droppedFile.relativePath, fileEntry);
        }
      }
    }
    chargeFile(){
      this.loading = true
      this.http.post('http://198.27.127.186:8000/upload/create', this.path).subscribe((res:any) => {
        if (res.status) {
          this.getMaterials()
        }
      })
    }
    // AGREGAR CONTENIDO
    registerMaterials()
    {
      if(this.editMaterialsForm.value.id)
      {
        // UPDATE MATERIALS
        const materials={
          idmaterial: this.editMaterialsForm.value.id,
          code: this.editMaterialsForm.value.code,
          name: this.editMaterialsForm.value.name,
          unit: this.editMaterialsForm.value.unit,
          price: this.editMaterialsForm.value.price,
          category: this.editMaterialsForm.value.category
        }
        this.materialsService.putMaterial(materials).subscribe( (res:any) => {
          this.getMaterials()
          if (res.status) {
              this.response = 'El material fue actualizado!'
              this.alert = 'alert alert-info'
            }
          })
        }
      else
      {
        // CREATE MATERIALS
        const newMaterial = {
          code: this.editMaterialsForm.value.code,
          name: this.editMaterialsForm.value.name,
          unit: this.editMaterialsForm.value.unit,
          price: this.editMaterialsForm.value.price,
          category: this.editMaterialsForm.value.category
          };
            this.materialsService.registerMaterials(newMaterial).subscribe( (res:any) => {
            this.getMaterials()
            this.response = 'El material se registro con exito!'
            this.alert = 'alert alert-success'
        })
      }
    }
    // EDITAR TABLAS
    editMaterials(materials:any){
      this.editMaterialsForm.setValue({
          id:  materials.idmaterial,
          name: materials.name,
          code: materials.code,
          unit: materials.unit,
          price:materials.price,
          category:materials.idcategory,
        })
      this.showModEdit = true
    }
    deleteMaterial(materials:any){
      this.deleteMaterialsForm.setValue({
          idmaterial:materials.idmaterial
      })
      this.showModEdit = true
    }
    deleteMaterials(){
      // ENVIAR DATOS(this.materials) AL BACKEND PARA GUARDAR
      this.materialsService.deleteMaterial(this.deleteMaterialsForm.value.idmaterial).subscribe((res:any)=>{
          this.getMaterials()
          if(res.status){
            this.response = 'El material se elimino de forma permanente!'
            this.alert = 'alert alert-danger'
          }
      })
    }

    // BUSQUEDA EN DATATABLE
    find(words:any){
      // console.log(words);
      let obj = { letra: words};
      this.materialsService.findMaterials(obj).subscribe((res:any) =>{
          this.materials = res.materials
      })
    }
}
