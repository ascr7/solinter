import { Component, ViewChild} from '@angular/core';
import { ModalDirective} from 'ngx-bootstrap/modal';
import { SchedulesvisitsService }  from "../../services/schedulesvisits.service";
// PROPIEDADES PARA FORMULARIOS
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { ClientService } from '../../services/client.service';
import { UserService } from '../../services/user.service';
import { LoginService } from '../../services/login.service';
import CryptoJS from "crypto-js";
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'visits',
  templateUrl: 'visits.component.html'
})
export class VisitsComponent {
  
  // MODAL PARA LAS TABLAS
  @ViewChild('myModal', { static: false }) public myModal: ModalDirective;
  @ViewChild('infoModal', { static: false }) public infoModal: ModalDirective;
  
  // INICIALIZAR VARIABLES  
  public clients:any;
  public visits:any;
  public users:any;
  public showModEdit = false;
  public response:any;
  public alert:any;
  
  // INICIALIZAR CAMPOS DEL FORMULARIO
  public editVisitsForm: FormGroup // NOMBRE DEL FOMULARIO
  public deleteVisitsForm: FormGroup
  public client: AbstractControl
  public descrioption: AbstractControl
  public user: AbstractControl
  public date: AbstractControl
  public time: AbstractControl
  
  public loading = false;

  constructor(
    public schedulesvisitsService: SchedulesvisitsService,
    public clientService: ClientService,
    public userService: UserService,
    public loginService: LoginService,
    private formBuilder: FormBuilder,
    public demoService: NgxUiLoaderService,
    
    ){
      // VALIDACIONES BACKEND
      this.editVisitsForm = this.formBuilder.group({
        id: ['' ],
        client: ['', Validators.required ],
        descrioption: ['', Validators.required ],
        userid: ['', Validators.required ],
        date: ['', Validators.required ],
        time: ['', Validators.required ],
      })

      this.deleteVisitsForm = this.formBuilder.group({
        idvisit: ['']
      }) 
      
      // VALIDACIONES FRONTEND
      this.client = this.editVisitsForm.controls['client']
      this.descrioption = this.editVisitsForm.controls['descrioption']
      this.user = this.editVisitsForm.controls['user']
      this.date = this.editVisitsForm.controls['date']
      this.time = this.editVisitsForm.controls['time']
    }
    
    // INICIALIZAR VARIABLES A USAR
    ngOnInit(){      
        this.getClients(),
        this.getUsers(),
        this.getVisits()        
    }

    // MOSTRAR TABLA VISITAS SEGUN AUTH
    getVisits(){
      let obj = { 
          iduser: JSON.parse(CryptoJS.AES.decrypt(localStorage.getItem('sessionId'), 'sigma7').toString(CryptoJS.enc.Utf8)),
          typeUser: JSON.parse(CryptoJS.AES.decrypt(localStorage.getItem('sessionTypeUser'), 'sigma7').toString(CryptoJS.enc.Utf8))
        }
        this.loading = true
        this.schedulesvisitsService.getVisits(obj).subscribe((res:any) =>{ // BACKEND FULL
            this.visits = res,
            this.loading = false
            // console.log(typeof(res))
      })
    }

    // MOSTRAR CLIENTES PARA INPUT SELECT DEL MODAL
    getClients(){
      this.clientService.getClients().subscribe((res:any) =>{
        // console.log(res)
        this.clients = res
      })
    }

    // MOSTRAR USUARIOS PARA INPUT SELECT DEL MODAL
    getUsers(){
      this.userService.getUsers().subscribe((res:any) =>{
        // console.log(res)
        this.users = res
      })
    }
    
    clearForm(){
      this.editVisitsForm.reset();
    }

    // AGREGAR CONTENIDO
    registerVisits()
    {
      if(this.editVisitsForm.value.id)
      { 
        // UPDATE VISITAS
        const visits={
          idSchedule: this.editVisitsForm.value.id,
          ClientName: this.editVisitsForm.value.client,
          DescSchedule:  this.editVisitsForm.value.descrioption,
          UserId: this.editVisitsForm.value.userid,
          ScheduleDate: this.editVisitsForm.value.date,
          ScheduleTime: this.editVisitsForm.value.time,
        }
        
        // ENVIAR DATOS(visits) AL BACKEND PARA GUARDAR
        this.schedulesvisitsService.putSchedule(visits).subscribe( (res:any) => {
          this.getVisits()
          if (res.status){
            this.response = 'La visita fue actualizada!'
            this.alert = 'alert alert-info'
            }
          })
      }
      else
      { 
        // CREATE VISITAS
        const newVisit = {
          ClientName: this.editVisitsForm.value.client,
          DescSchedule:  this.editVisitsForm.value.descrioption,
          UserId: this.editVisitsForm.value.userid,
          ScheduleDate: this.editVisitsForm.value.date,
          ScheduleTime: this.editVisitsForm.value.time
        };
  
        this.schedulesvisitsService.registerSchedules(newVisit).subscribe( (res:any) => {
            this.getVisits()
            this.response = 'La visita se registro con exito!'
            this.alert = 'alert alert-success'
        })
      }
    }

    // EDITAR TABLAS
    editVisits(visits:any){
        // ASIGNAR VALORES EN EL FORMULARIO MODAL 
        this.editVisitsForm.setValue({ 
            id: visits.schedule_id,
            client: visits.idclient,
            descrioption: visits.descrioption,
            userid: visits.user_userid,
            date: visits.datevisit,
            time: visits.timevisit
        })
        this.showModEdit = true                
    }
  
    deleteVisit(visits:any){
      this.deleteVisitsForm.setValue({ 
          idvisit:visits.schedule_id
      })
      this.showModEdit = true
    }
  
    deleteVisits(){
      this.schedulesvisitsService.deleteSchedule(this.deleteVisitsForm.value.idvisit).subscribe((res:any)=>{
          this.getVisits()
          if(res.status){
            this.response = 'La visita se elimino de forma permanente!'
            this.alert = 'alert alert-danger'
          }
        })
    }

    // BUSQUEDA EN DATATABLE
    find(words:any){
      // console.log(words);
      let obj = { letra: words};
      this.schedulesvisitsService.findVisits(obj).subscribe((res:any) =>{
          this.visits = res.visits
      })    
    }

}

