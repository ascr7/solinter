import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { SchedulesvisitsService } from "../../../services/schedulesvisits.service";
import { ToolsService } from "../../../services/tools.service";
import {
  FormGroup,
  FormBuilder,
  AbstractControl,
  Validators
} from "@angular/forms";
import pdfMake from "../../../../../bower_components/pdfmake/build/pdfmake";
import pdfFonts from "../../../../../bower_components/pdfmake/build/vfs_fonts";
import { Image } from '@ks89/angular-modal-gallery';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class DetailComponent implements OnInit {
  public type = [
    { tipo: "Cotización corta", value: 1 },
    { tipo: "Cotización larga", value: 2 }
  ];
  public viewAll = false;
  public visit: any;
  public sections: any;
  public materials: any;
  public full: any;
  public machinaries: any;
  public tools = [];
  public workforce = [];
  public Totalfull = 0;
  public Total = 0;
  public TotalT = 0;
  public TotalT2 = 0;
  public TotalW = 0;
  public TotalW2 = 0;
  public TotalMT = 0;
  public TotalMT2 = 0;
  public TotalMC = 0;
  public TotalMC2 = 0;
  public countMC = 0;
  public countMT = 0;
  public countT = 0;
  public countW = 0;
  isCollapsed: boolean = true;
  public PorcentageMC = 0;
  public PorcentageMT = 0;
  public PorcentageT = 0;
  public PorcentageW = 0;
  public PorMC = false;
  public PorMT = false;
  public PorT = false;
  public PorW = false;
  public cotizaForm: FormGroup; // NOMBRE DEL FOMULARIO
  public tipo: AbstractControl;
  public descripcion: AbstractControl;
  public personal: AbstractControl;
  public formapago: AbstractControl;
  public generalidades: AbstractControl;
  public tiempodeentrega: AbstractControl;
  public observaciones: AbstractControl;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private service: SchedulesvisitsService,
    public toolservice: ToolsService
  ) {
    this.cotizaForm = this.formBuilder.group({
      id: [""],
      tipo: ["", Validators.required],
      descripcion: ["", Validators.required],
      generalidades: ["", Validators.required],
      personal: ["", Validators.required],
      formapago: ["", Validators.required],
      tiempodeentrega: ["", Validators.required],
      observaciones: ["", Validators.required]
    });
    this.tipo = this.cotizaForm.controls["tipo"];
    this.descripcion = this.cotizaForm.controls["descripcion"];
    this.generalidades = this.cotizaForm.controls["generalidades"];
    this.personal = this.cotizaForm.controls["personal"];
    this.formapago = this.cotizaForm.controls["formapago"];
    this.observaciones = this.cotizaForm.controls["observaciones"];
    this.tiempodeentrega = this.cotizaForm.controls["tiempodeentrega"];
  }

  ngOnInit() {
    this.getVisitInfo();
  }

  getVisitInfo() {
    this.service
      .getSchedule(this.route.snapshot.paramMap.get("id"))
      .subscribe((res: any) => {
        this.visit = res.data.visit;
        this.sections = res.data.sections;
        for (let i = 0; i < this.sections.length; i++) {
          let images: Image[] = [];
          const e = this.sections[i];
          images.push(new Image(e.idsection,{img: e.mockup_picture_uri, description: "Bosquejo"}))
          for (let j = 0; j < e.photos.length; j++) {
            const p = e.photos[j];
            let im = new Image(p.photo_id,{img: p.photo_uri})
            images.push(im);
          }
          e.photos = images;
        }
        for (let i = 0; i < res.data.sections.length; i++) {
          const e = res.data.sections[i];
          if (e.materials.length > 0) {
            this.getMaterials(e.materials);
          }
          if (e.machinaries.length > 0) {
            this.getMachinaries(e.machinaries);
          }
          if (e.tools.length > 0) {
            this.getTools(e.tools);
          }
          if (e.workforce.length > 0) {
            this.getWorkforces(e.workforce);
          }
        }
        this.getTotalfull();
      });
  }

  async CreateCot() {
    const cotizacion = {
      id: this.route.snapshot.paramMap.get("id"),
      tipo: this.cotizaForm.value.tipo,
      generalidades: this.cotizaForm.value.generalidades,
      personal: this.cotizaForm.value.personal,
      formapago: this.cotizaForm.value.formapago,
      observaciones: this.cotizaForm.value.observaciones,
      tiempodeentrega: this.cotizaForm.value.tiempodeentrega,
      descripcion: this.cotizaForm.value.descripcion
    };
    this.service.createQuotation(cotizacion).subscribe(res => {
      pdfMake.createPdf(res).download();
    });
  }

  cotiLarge(e) {
    if (e.target.value == 2) {
      this.viewAll = false;
    } else if (e.target.value == 1) {
      this.viewAll = true;
    }
  }

  porcentage(e) {
    if (e.target.name == "materiales") {
      for (let i = 0; i < this.sections.length; i++) {
        const s = this.sections[i];
        let materialSelected = {
          code_visit: this.route.snapshot.paramMap.get("id"),
          materials: s.materials,
          porcentage: this.PorcentageMT,
          id_section: s.idsection
        };
        this.PorMT = true;
        this.toolservice.putSelected(materialSelected).subscribe();
      }
    } else if (e.target.name == "maquinarias") {
      for (let i = 0; i < this.sections.length; i++) {
        const s = this.sections[i];
        let machinarySelected = {
          code_visit: this.route.snapshot.paramMap.get("id"),
          materials: s.machinaries,
          porcentage: this.PorcentageMC,
          id_section: s.idsection
        };
        this.PorMC = true;
        this.toolservice.putSelected(machinarySelected).subscribe();
      }
    } else if (e.target.name == "manodeobra") {
      for (let i = 0; i < this.sections.length; i++) {
        const s = this.sections[i];
        let workfSelected = {
          code_visit: this.route.snapshot.paramMap.get("id"),
          materials: s.workforce,
          porcentage: this.PorcentageW,
          id_section: s.idsection
        };
        this.PorW = true;
        this.toolservice.putSelected(workfSelected).subscribe();
      }
    } else if (e.target.name == "herramientas") {
      for (let i = 0; i < this.sections.length; i++) {
        const s = this.sections[i];
        let toolSelected = {
          code_visit: this.route.snapshot.paramMap.get("id"),
          materials: s.tools,
          porcentage: this.PorcentageT,
          id_section: s.idsection
        };
        this.PorW = true;
        this.toolservice.putSelected(toolSelected).subscribe();
      }
    }
    this.getTotalfull();
  }
  getTotalfull() {
    if (
      this.PorMT == true ||
      this.PorMC == true ||
      this.PorT == true ||
      this.PorW == true
    ) {
      this.Totalfull =
        this.TotalMT +
        (this.TotalMT * this.PorcentageMT) / 100 +
        (this.TotalMC + (this.TotalMC * this.PorcentageMC) / 100) +
        (this.TotalT + (this.TotalT * this.PorcentageT) / 100) +
        (this.TotalW + (this.TotalW * this.PorcentageW) / 100);
    } else {
      this.Totalfull = this.TotalMT + this.TotalMC + this.TotalT + this.TotalW;
    }
  }
  getMaterials(materials: any) {
    for (let i = 0; i < materials.length; i++) {
      const e = materials[i];
      this.countMT += e.count;
      this.PorcentageMT = e.porcentage;
      this.PorMT = true;
      this.TotalMT += e.precio * e.count;
      this.TotalMT2 += e.precio;
    }
  }
  getMachinaries(machinaries: any) {
    for (let i = 0; i < machinaries.length; i++) {
      const e = machinaries[i];
      this.countMC += e.count;
      this.PorcentageMC = e.porcentage;
      this.PorMC = true;
      this.TotalMC += e.precio * e.count;
      this.TotalMC2 += e.precio;
    }
  }

  getTools(tools: any) {
    for (let i = 0; i < tools.length; i++) {
      const e = tools[i];
      this.countT += e.count;
      this.PorcentageT = e.porcentage;
      this.PorT = true;
      this.TotalT += e.precio * e.count;
      this.TotalT2 += e.precio;
    }
  }
  getWorkforces(workforce: any) {
    for (let i = 0; i < workforce.length; i++) {
      const e = workforce[i];
      this.countW += e.count;
      this.PorcentageW = e.porcentage;
      this.PorW = true;
      this.TotalW += e.precio * e.count;
      this.TotalW2 += e.precio;
    }
  }
}
