import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisitComponent } from './visit/visit.component';
import { DetailComponent } from "./detail/detail.component"

const routes: Routes = [
  {path: '',component: VisitComponent,data: {title: 'Gestion de Visitas'}},
  {path: 'visita/:id',component: DetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitsRoutingModule {}
