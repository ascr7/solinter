import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';
import { GalleryModule } from '@ks89/angular-modal-gallery';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { VisitsComponent } from './visits.component';
import { VisitsRoutingModule } from './visits-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { VisitComponent } from './visit/visit.component';
import { DetailComponent } from "./detail/detail.component";
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    VisitsRoutingModule,
    NgxPaginationModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    FilterPipeModule,
    GalleryModule
  ],
  declarations: [
    VisitsComponent,
    VisitComponent,
    DetailComponent,
   ]
})
export class VisitsModule { }
