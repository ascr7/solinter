import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class WorkforceService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService)
  {
  }

  ngOnInit()
  {
  }
  
  getWorkforces()
  {
    return this.http.get(this.URL_API+"/workforce")
  }

  findWorkforces(obj:any)
  {
    return this.http.post(this.URL_API+"/workforce/find", obj)
  }

  registerWorkforces(nameWorkforce: any)
  {
    return this.http.post(this.URL_API+"/workforce/create", nameWorkforce)
  }

  putWorkforce(workforce: any){
    return this.http.put(this.URL_API + `/workforce/${workforce._id}`, workforce)
  }

  deleteWorkforce(_id: string)
  {
    return this.http.delete(this.URL_API + "/workforce"+ `/${_id}`)
  }

}
