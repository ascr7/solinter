import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService)
  {
  }

  ngOnInit()
  {
  }
  
  getTools()
  {
    return this.http.get(this.URL_API+"/herramienta")
  }

  putSelected(toolSelected)
  {
    return this.http.put(this.URL_API+`/serviceorder/selectedput/${toolSelected.code_visit}`, toolSelected)
  }

  findTools(obj:any)
  {
    return this.http.post(this.URL_API+"/herramienta/find", obj)
  }

  registerTools(nameTool: any)
  {
    return this.http.post(this.URL_API+"/herramienta/create", nameTool)
  }

  putTool(tools: any){
    return this.http.put(this.URL_API + `/herramienta/${tools._id}`, tools)
  }

  deleteTool(_id: string)
  {
    return this.http.delete(this.URL_API + "/herramienta"+ `/${_id}`)
  }

}
