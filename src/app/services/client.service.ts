import { Injectable } from '@angular/core';
import { ConfigService } from './config-service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService)
  {
  }
  ngOnInit()
  {
  }

  getClients()
  {
    return this.http.get(this.URL_API+"/clients")
  }

  findClients(obj:any)
  {
    return this.http.post(this.URL_API+"/clients/find", obj)
  }

  searchsClients(client: any)
  {
    return this.http.post(this.URL_API+"/clients/search", client)
  }

  registerClient(client: any)
  {
    return this.http.post(this.URL_API+"/clients/create", client)
  }

  putClient(client: any)
  {
    return this.http.put(this.URL_API + `/clients/${client._id}`, client)
  }

  deleteClient(_id: string)
  {
    return this.http.delete(this.URL_API + "/clients"+ `/${_id}`)
  }
}
