import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public storage:any;
  
  constructor(
    private loginService: LoginService
  ) { }

}
