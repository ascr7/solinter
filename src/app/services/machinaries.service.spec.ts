import { TestBed } from '@angular/core/testing';

import { MachinariesService } from './machinaries.service';

describe('MachinariesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MachinariesService = TestBed.get(MachinariesService);
    expect(service).toBeTruthy();
  });
});
