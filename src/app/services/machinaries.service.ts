import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: "root"
})
export class MachinariesService {
  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}

  ngOnInit() {}

  getMachinaries() {
    return this.http.get(this.URL_API + "/maquinaria");
  }

  findMachinary(obj: any) {
    return this.http.post(this.URL_API + "/maquinaria/find", obj);
  }

  registerMachinaries(nameMachinary: any) {
    return this.http.post(this.URL_API + "/maquinaria/create", nameMachinary);
  }

  putMachinary(machinary: any) {
    return this.http.put(
      this.URL_API + `/maquinaria/${machinary._id}`,
      machinary
    );
  }

  deleteMachinary(_id: string) {
    return this.http.delete(this.URL_API + "/maquinaria" + `/${_id}`);
  }
}
