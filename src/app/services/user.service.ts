import { Injectable, OnInit } from "@angular/core";
import { ConfigService } from "./config-service";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserService implements OnInit {
  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  ngOnInit() {}

  getUsers() {
    return this.http.get(this.URL_API + "/users");
  }

  getUser(UserId: string) {
    return this.http.get(this.URL_API + `/users/${UserId}`);
  }

  getUserType() {
    return this.http.get(this.URL_API + "/usertype");
  }

  findUsers(obj: any) {
    return this.http.post(this.URL_API + "/users/find", obj);
  }

  registerUsers(nameUser: any) {
    return this.http.post(this.URL_API + "/users/create", nameUser);
  }

  putUser(user: any) {
    // console.log(user);
    return this.http.put(this.URL_API + `/users/${user._id}`, user);
  }

  deleteUser(_id: string) {
    return this.http.delete(this.URL_API + "/users" + `/${_id}`);
  }
}
