import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";
import * as io from "socket.io-client";

@Injectable({
  providedIn: "root"
})
export class SchedulesvisitsService implements OnInit {
  readonly URL_API = this.config.getConfig().bussinesServer.url;
  private socket;
  constructor(private http: HttpClient, private config: ConfigService) {
    this.socket = io(this.URL_API);
  }
  ngOnInit() {}

  createQuotation(cotizacion: any) {
    return this.http.post(this.URL_API + "/pdf/pdf", cotizacion);
  }

  getSchedules() {
    return this.http.get(this.URL_API + "/schedulevisits");
  }

  getSchedule(id: String) {
    return this.http.get(this.URL_API + `/schedulevisits/${id}`);
  }

  getVisits(obj: any) {
    return this.http.post(this.URL_API + "/schedulevisits/webvisits", obj);
  }

  getVisitsDay() {
    return this.http.get(this.URL_API + "/schedulevisits/webvisitsday");
  }

  getWebVisitsPen() {
    return this.http.get(this.URL_API + "/schedulevisits/visitsPen");
  }

  getSchedulePen(user:any){
    return this.http.post(this.URL_API+"/schedulevisits/visitspen", user)
  }

  getVisitsPen(iduser: string) {
    return this.http.get(
      this.URL_API + `/schedulevisits/visitsPen/?iduser=${iduser}`
    );
  }

  findVisits(obj: any) {
    return this.http.post(this.URL_API + "/schedulevisits/find", obj);
  }

  getClients() {
    return this.http.get(this.URL_API + "/clients");
  }

  registerSchedules(schedule: any) {
    this.socket.emit("newschedule", schedule);
    return this.http.post(this.URL_API + "/schedulevisits/create", schedule);
  }

  putSchedule(schedule: any) {
    return this.http.put(
      this.URL_API + `/schedulevisits/${schedule._id}`,
      schedule
    );
  }

  deleteSchedule(_id: string) {
    return this.http.delete(this.URL_API + "/schedulevisits" + `/${_id}`);
  }
}
