import { Injectable } from '@angular/core';
import { configuration } from '../containers/configuration/configuration';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  config = configuration
  message =  Message

  constructor() { }

  getConfig()
  {
    return this.config
  }

  getMessage()
  {
    return this.message
  }
  
}
