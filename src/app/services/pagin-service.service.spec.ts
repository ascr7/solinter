import { TestBed } from '@angular/core/testing';

import { PaginServiceService } from './pagin-service.service';

describe('PaginServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaginServiceService = TestBed.get(PaginServiceService);
    expect(service).toBeTruthy();
  });
});
