import { TestBed } from '@angular/core/testing';

import { TypeuserService } from './typeuser.service';

describe('TypeuserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeuserService = TestBed.get(TypeuserService);
    expect(service).toBeTruthy();
  });
});
