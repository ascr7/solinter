interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

//  ------------ ------------- MENU PARA ADMINISTRADOR - SECRETARIA ------------ ------------

export const navItems: NavData[] = [
  {
    name: 'Inicio',
    url: '/inicio',
    icon: 'cui-home',
  },
  {
    name: 'Empresas',
    url: '/empresas',
    icon: 'cui-chart',
  },
  {
    name: 'Usuarios',
    url: '/usuarios',
    icon: 'cui-people'
  },
  {
    name: 'Visitas',
    url: '/visitas',
    icon: 'cui-calendar'
  },
  {
    name: 'Materiales',
    url: '/materiales',
    icon: 'cui-cog'
  },
  {
    name: 'Herramientas',
    url: '/herramientas',
    icon: 'cui-briefcase'
  },
  {
    name: 'Maquinaria',
    url: '/maquinaria',
    icon: 'cui-wrench'
  },
  {
    name: 'Mano de Obra',
    url: '/obra',
    icon: 'cui-thumb-up'
  },
];

//  ------------ ------------- MENU PARA TRABAJADOR ------------ ------------

export const navItems2: NavData[] = [
  {
    name: 'Inicio',
    url: '/inicio',
    icon: 'cui-home',
  },
  {
    name: 'Gestion Visitas',
    url: '/visitas',
    icon: 'cui-people'
  },
  {
    name: 'Gestion Materiales',
    url: '/materiales',
    icon: 'cui-cog'
  },

];
